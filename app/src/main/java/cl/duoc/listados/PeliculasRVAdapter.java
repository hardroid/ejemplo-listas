package cl.duoc.listados;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by DUOC on 29-04-2017.
 */

public class PeliculasRVAdapter extends RecyclerView.Adapter<PeliculasRVViewHolder> {
    private ArrayList<Pelicula> dataSource;

    public PeliculasRVAdapter(ArrayList<Pelicula> dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public PeliculasRVViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = inflater.inflate(R.layout.peliculas_item, parent, false);

        PeliculasRVViewHolder holder = new PeliculasRVViewHolder(parent.getContext(), convertView);
        return holder;
    }

    @Override
    public void onBindViewHolder(PeliculasRVViewHolder holder, int position) {
        holder.bindPelicula(dataSource.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }
}

package cl.duoc.listados;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class RecyclerViewActivity extends AppCompatActivity {

    private RecyclerView rvPeliculas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);
        rvPeliculas = (RecyclerView)findViewById(R.id.rvPeliculas);

        PeliculasRVAdapter adapter = new PeliculasRVAdapter(BaseDeDatos.getInstance().getPeliculas());
        rvPeliculas.setAdapter(adapter);
        rvPeliculas.setLayoutManager(
                new GridLayoutManager(this,1));
    }
}

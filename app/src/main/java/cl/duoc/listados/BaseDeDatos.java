package cl.duoc.listados;

import java.util.ArrayList;

/**
 * Created by DUOC on 29-04-2017.
 */

public class BaseDeDatos {
    private static BaseDeDatos instance;

    private BaseDeDatos(){}

    public static BaseDeDatos getInstance(){
        if(instance == null){
            instance = new BaseDeDatos();
        }
        return instance;
    }

    public ArrayList<Pelicula> getPeliculas() {

        Pelicula cadenaPerpetua = new Pelicula("Cadena perpetua",
                "1994",
                9.2,
                "https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_UY67_CR0,0,45,67_AL_.jpg",
                true,
                1);

        Pelicula padrinoUno = new Pelicula("El padrino",
                "1972",
                9.2,
                "https://images-na.ssl-images-amazon.com/images/M/MV5BZTRmNjQ1ZDYtNDgzMy00OGE0LWE4N2YtNTkzNWQ5ZDhlNGJmL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR1,0,45,67_AL_.jpg",
                true,
                2);

        Pelicula padrinoDos = new Pelicula("El padrino II",
                "1974",
                9.0,
                "https://images-na.ssl-images-amazon.com/images/M/MV5BMjZiNzIxNTQtNDc5Zi00YWY1LThkMTctMDgzYjY4YjI1YmQyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR1,0,45,67_AL_.jpg",
                true,
                3);

        ArrayList<Pelicula> peliculas = new ArrayList<>();
        peliculas.add(cadenaPerpetua);
        peliculas.add(padrinoUno);
        peliculas.add(padrinoDos);

        for (int x = 4; x < 10000; x++){
            Pelicula aux = new Pelicula("El padrino II",
                    "1974",
                    9.0,
                    "https://images-na.ssl-images-amazon.com/images/M/MV5BMjZiNzIxNTQtNDc5Zi00YWY1LThkMTctMDgzYjY4YjI1YmQyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR1,0,45,67_AL_.jpg",
                    false,
                    4);
            peliculas.add(aux);
        }

        return peliculas;

    }
}

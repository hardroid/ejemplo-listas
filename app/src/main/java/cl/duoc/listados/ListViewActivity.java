package cl.duoc.listados;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class ListViewActivity extends AppCompatActivity {

    private ListView listaIMBD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        listaIMBD = (ListView) findViewById(R.id.listaIMBD);
        PeliculasAdapter adapter = new PeliculasAdapter(this, BaseDeDatos.getInstance().getPeliculas());
        listaIMBD.setAdapter(adapter);
    }



}

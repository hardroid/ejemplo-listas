package cl.duoc.listados;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DUOC on 29-04-2017.
 */

public class PeliculasAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Pelicula> dataSource;

    public PeliculasAdapter(Context context, ArrayList<Pelicula> dataSource) {
        this.context = context;
        this.dataSource = dataSource;
    }

    @Override
    public int getCount() {
        return dataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.peliculas_item, parent, false);
            holder = new ViewHolder();
            holder.imgPelicula = (ImageView) convertView.findViewById(R.id.imgPelicula);
            holder.tvRank = (TextView) convertView.findViewById(R.id.tvRank);
            holder.tvNombre = (TextView) convertView.findViewById(R.id.tvNombre);
            holder.tvEstreno = (TextView) convertView.findViewById(R.id.tvEstreno);
            holder.tvRating = (TextView) convertView.findViewById(R.id.tvRating);
            holder.btnFavoritos = (ImageView) convertView.findViewById(R.id.btnFavoritos);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        // Set data into the view.


        Pelicula item = this.dataSource.get(position);

        Picasso.with(context).load(item.getUrlImagen()).into(holder.imgPelicula);
        holder.tvRank.setText(item.getRank()+"");
        holder.tvNombre.setText(item.getNombre());
        holder.tvEstreno.setText(item.getEstreno());
        holder.tvRating.setText(item.getRating()+"");
        if(item.isFavorito()){
            holder.btnFavoritos.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_toggle_star_24));
        }else{
            holder.btnFavoritos.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_toggle_star_outline_24));
        }

        return convertView;
    }

    static class ViewHolder {
        private ImageView imgPelicula;
        private TextView tvRank;
        private TextView tvNombre;
        private TextView tvEstreno;
        private TextView tvRating;
        private ImageView btnFavoritos;
    }
}


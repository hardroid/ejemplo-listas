package cl.duoc.listados;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by DUOC on 29-04-2017.
 */

public class PeliculasRVViewHolder extends RecyclerView.ViewHolder {

    private ImageView imgPelicula;
    private TextView tvRank;
    private TextView tvNombre;
    private TextView tvEstreno;
    private TextView tvRating;
    private ImageView btnFavoritos;

    private Context context;

    public PeliculasRVViewHolder(Context context, View itemView) {
        super(itemView);
        this.context = context;
        imgPelicula = (ImageView) itemView.findViewById(R.id.imgPelicula);
        tvRank = (TextView) itemView.findViewById(R.id.tvRank);
        tvNombre = (TextView) itemView.findViewById(R.id.tvNombre);
        tvEstreno = (TextView) itemView.findViewById(R.id.tvEstreno);
        tvRating = (TextView) itemView.findViewById(R.id.tvRating);
       btnFavoritos = (ImageView) itemView.findViewById(R.id.btnFavoritos);
    }

    public void bindPelicula(Pelicula item) {

        Picasso.with(context).load(item.getUrlImagen()).into(imgPelicula);
        tvRank.setText(item.getRank()+"");
        tvNombre.setText(item.getNombre());
        tvEstreno.setText(item.getEstreno());
        tvRating.setText(item.getRating()+"");
        if(item.isFavorito()){
            btnFavoritos.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_toggle_star_24));
        }else{
            btnFavoritos.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_toggle_star_outline_24));
        }
    }
}
